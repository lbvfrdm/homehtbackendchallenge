package home

import java.time.{OffsetDateTime, ZoneId}
import java.util.concurrent.ConcurrentHashMap
import java.util.concurrent.atomic.AtomicInteger

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import akka.http.scaladsl.server._
import akka.http.scaladsl.unmarshalling.Unmarshaller
import akka.stream.ActorMaterializer
import spray.json.{DefaultJsonProtocol, DeserializationException, JsString, JsValue, RootJsonFormat}

import scala.io.StdIn

object Task extends Directives with DefaultJsonProtocol with SprayJsonSupport {

  /*
    $sbt run

    For the sake of simplicity, all the code is in one file.
   */

  //classes
  case class New(contractId: Int,
                 description: String,
                 value: Int,
                 isImported: Boolean)

  case class Update(itemId: Int,
                    description: String,
                    value: Int)

  case class Item(id: Int,
                  contractId: Int,
                  description: String,
                  value: Int,
                  time: OffsetDateTime,
                  isImported: Boolean,
                  createdAt: OffsetDateTime,
                  updatedAt: OffsetDateTime,
                  isDeleted: Boolean)

  case class Fetch(sum: Int, items: List[Item])

  //json
  implicit object OffsetDateTimeFormat extends RootJsonFormat[OffsetDateTime] {
    def write(c: OffsetDateTime) = JsString(c.toString)
    def read(value: JsValue): OffsetDateTime = {
      value match {
        case JsString(name) => OffsetDateTime.parse(name)
        case _ => throw DeserializationException("date expected")
      }
    }
  }

  implicit val newFormat = jsonFormat4(New)
  implicit val updateFormat = jsonFormat3(Update)
  implicit val itemFormat = jsonFormat9(Item)
  implicit val fetchFormat = jsonFormat2(Fetch)

  //store
  class TemporaryStorage {

    private val map = new ConcurrentHashMap[Integer, Item]()
    private val idsSequence = new AtomicInteger(1)

    def add(elem: New): Either[String, Item] =  {

        val nextId = idsSequence.getAndIncrement()
        val now = currentDateTime
        val item = Item(nextId, elem.contractId, elem.description, elem.value, now, elem.isImported, now, now, false)
        Option(map.putIfAbsent(nextId, item)).map(_ => "storage is full").toLeft(item)
    }

    def update(elem: Update): Either[String, Item] = {

        val now = currentDateTime
        val result = map.computeIfPresent(elem.itemId, (_, value)=>{
          if(!value.isDeleted) value.copy(description = elem.description, value = elem.value, updatedAt = now) else value
        })

        Option(result) match {
          case None => Left("item does not exist")
          case Some(item) if item.isDeleted => Left("item was removed")
          case Some(item) => Right(item)
        }
    }

    def remove(itemId: Int): Either[String, String] = {
      val result = map.computeIfPresent(itemId, (_, item) => {item.copy(isDeleted = true)})
      Option(result).map(_ => "item has been removed").toRight("item does not exist")
    }

    def getByContractId(key: Int, startDate: OffsetDateTime, endDate: OffsetDateTime): Fetch = {
      import collection.JavaConverters._
      val payments = map.values().asScala.filter(item =>  item.contractId == key &&
                                                          //!item.isDeleted &&  //show deleted items too
                                                          item.updatedAt.isAfter(startDate) &&
                                                          item.updatedAt.isBefore(endDate))
                                          .toList
                                          .sortBy(_.id)(Ordering[Int].reverse)
      val sum = payments.map(_.value).sum
      Fetch(sum, payments)
    }
  }


  //web
  class TaskRest(storage: TemporaryStorage = new TemporaryStorage){

    implicit val offsetDateTimeUnmarshaller: Unmarshaller[String, OffsetDateTime] = Unmarshaller.strict[String, OffsetDateTime](OffsetDateTime.parse)

    val route =
      (path("fetch") & get & parameters('contractId.as[Int],
                                              'startDate.as[java.time.OffsetDateTime] ,
                                              'endDate.as[java.time.OffsetDateTime] )) { (contractId, startDate, endDate) =>

              complete(storage.getByContractId(contractId, startDate, endDate))

      } ~ (path("add") & post & entity(as[New])) { item =>

              complete(storage.add(item))

      } ~ (path("update") & post & entity(as[Update])) { item =>

              complete(storage.update(item))

      } ~ (path("remove") & delete & parameters('itemId.as[Int] )) { itemId =>

              complete(storage.remove(itemId))
      }
  }

  def currentDateTime: OffsetDateTime = OffsetDateTime.now(ZoneId.of("UTC"))

  def main(args: Array[String]): Unit = {

    implicit val system = ActorSystem("home-rest")
    implicit val materializer = ActorMaterializer()
    implicit val executionContext = system.dispatcher

    val host = "0.0.0.0"
    val port = 80

    val route = new TaskRest().route

    val bindingFuture = Http().bindAndHandle(route, host, port)

    println(s"Server online\nPress RETURN to stop...")
    StdIn.readLine()
    bindingFuture
      .flatMap(_.unbind())
      .onComplete(_ => system.terminate())
  }
}
