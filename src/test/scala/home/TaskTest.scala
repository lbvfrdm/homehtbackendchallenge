package home

import java.time.OffsetDateTime

import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import akka.http.scaladsl.model.HttpEntity
import akka.http.scaladsl.model.MediaTypes.`application/json`
import akka.http.scaladsl.model.StatusCodes._
import akka.http.scaladsl.testkit.ScalatestRouteTest
import home.Task.{Fetch, Item, New, TaskRest, _}
import org.scalatest._

class TaskTest extends FlatSpec with GivenWhenThen with Matchers with OptionValues with EitherValues with ScalatestRouteTest with SprayJsonSupport {

  /*
    $sbt test

    basic tests
   */

   val now: OffsetDateTime = currentDateTime

   "store" can "add item" in {
        val storage = new TemporaryStorage
        val add = New(1, "Rent payment", 200, false)
        val storedItem = storage.add(add)
        val fetch = storage.getByContractId(add.contractId, now, now.plusMinutes(1))
        assert(fetch.items.size == 1)
        assert(storedItem.right.value == fetch.items.head)
  }

  it can "update item" in {
        val storage = new TemporaryStorage
        val add = New(1, "Rent payment", 200, false)
        val storedItem = storage.add(add)
        val update = Update(storedItem.right.value.id, "updated rent payment", 100)
        val updatedItem = storage.update(update)
        val fetch = storage.getByContractId(add.contractId, now, now.plusMinutes(1))
        assert(fetch.items.size == 1)
        assert(updatedItem.right.value == fetch.items.head)
  }

  it can "remove item" in {
        val storage = new TemporaryStorage
        val add = New(1, "Rent payment", 200, false)
        val storedItem = storage.add(add)
        storage.remove(storedItem.right.value.id)
        val fetch = storage.getByContractId(add.contractId, now, now.plusMinutes(1))
        assert(fetch.items.size == 1)
        assert(fetch.items.headOption.value.isDeleted)
  }

   "rest" can "add item" in {

        Given("rest api")
            val route = new TaskRest().route

        When("add item")
            val itemId = 1
            val contractId = 1
            val add = New(contractId, "Rent payment", 200, false)
            Post("/add", HttpEntity(`application/json`, add.toJson.toString())) ~> route ~> check {
              status shouldBe OK
              responseAs[Item] should matchPattern { case item: Item if item.id == itemId &&
                                                                        item.contractId == add.contractId &&
                                                                        item.value == add.value &&
                                                                        item.description == add.description=> }
            }

        Then("fetch should contain added item")
            val path = s"/fetch?contractId=${add.contractId}&startDate=$now&endDate=${now.plusMinutes(1)}"
            Get(path) ~> route ~> check {
              status shouldBe OK
              responseAs[Fetch] should matchPattern { case Fetch(sum, item :: Nil) if sum == add.value &&
                                                                                    item.id == itemId &&
                                                                                    item.contractId == add.contractId &&
                                                                                    item.value == add.value &&
                                                                                    item.description == add.description=> }
            }
  }

   it can "update item" in {

        Given("rest api")
            val route = new TaskRest().route

        When("add item")
            val itemId = 1
            val contractId = 1
            val add = New(contractId, "Rent payment", 200, false)
            Post("/add", HttpEntity(`application/json`, add.toJson.toString())) ~> route ~> check {
              status shouldBe OK
              responseAs[Item] should matchPattern { case item: Item if item.id == itemId &&
                                                                        item.contractId == add.contractId &&
                                                                        item.value == add.value &&
                                                                        item.description == add.description => }
            }

        When("update item")
            val update = Update(1, "updated rent payment", 100)
            Post("/update", HttpEntity(`application/json`, update.toJson.toString())) ~> route ~> check {
              status shouldBe OK
              responseAs[Item] should  matchPattern { case item: Item if item.id == itemId &&
                                                                         item.contractId == add.contractId &&
                                                                         item.value == update.value &&
                                                                         item.description == update.description => }
            }

        Then("fetch should contain updated item")
            val path = s"/fetch?contractId=${add.contractId}&startDate=$now&endDate=${now.plusMinutes(1)}"
            Get(path) ~> route ~> check {
              status shouldBe OK
              responseAs[Fetch] should matchPattern { case Fetch(sum, item :: Nil) if sum == update.value &&
                                                                                    item.id == itemId &&
                                                                                    item.contractId == add.contractId &&
                                                                                    item.value == update.value &&
                                                                                    item.description == update.description => }
            }
  }

   it can "remove item" in {

        Given("have rest api")
            val route = new TaskRest().route

        When("add item")
            val itemId = 1
            val contractId = 1
            val add = New(contractId, "Rent payment", 200, false)
            Post("/add", HttpEntity(`application/json`, add.toJson.toString())) ~> route ~> check {
              status shouldBe OK
              responseAs[Item] should matchPattern { case item: Item if item.id == itemId &&
                                                                        item.contractId == add.contractId &&
                                                                        item.value == add.value &&
                                                                        item.description == add.description => }
            }

        When("remove item")
            Delete(s"/remove?itemId=$itemId") ~> route ~> check {
              status shouldBe OK
              responseAs[String] shouldBe "item has been removed"
            }

        Then("fetch should contain item with delete flag")
            val path = s"/fetch?contractId=${add.contractId}&startDate=$now&endDate=${now.plusMinutes(1)}"
            Get(path) ~> route ~> check {
              status shouldBe OK
              responseAs[Fetch] should matchPattern { case Fetch(sum, item :: Nil) if sum == add.value &&
                                                                                    item.id == itemId &&
                                                                                    item.contractId == add.contractId &&
                                                                                    item.value == add.value &&
                                                                                    item.description == add.description &&
                                                                                    item.isDeleted == true => }
            }
  }
}